object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 448
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 16
    Top = 24
    Width = 225
    Height = 409
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 416
    Top = 24
    Width = 217
    Height = 409
    Lines.Strings = (
      'Memo2')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 288
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Length'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 288
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 288
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Contains'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 288
    Top = 192
    Width = 75
    Height = 25
    Caption = 'LowerCase'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 288
    Top = 248
    Width = 75
    Height = 25
    Caption = 'UpperCase'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 288
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
  end
end
